<?php require "logout.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>



    <title>Author Form</title>
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>

    <div class="container-fluid">

        <?php require "nav.php"; ?>
        <div class="row">
            <div class="col-lg-5 offset-lg-3 mt-5">

              <!--  php validation -->
              <?php 
                require "connection.php";
              
                        if(isset($_POST['submit'])){          
                        
                            $fristname = $_POST['firstname'];
                            $dob = $_POST['dob'];   
                            $gender = $_POST['gender'];
                            $address_ = $_POST['desc1'];
                            $mobile_no = $_POST['m_number'];
                            $description_ = $_POST['desc'];
                          
                            if($fristname =="" || $address_=="" || $dob=="" || $gender=="" || $mobile_no=="" || $description_=="" ){
                                
                                echo '<script type="text/javascript">';
                                echo ' alert("Palse Fill above data")'; 
                                echo '</script>';
                                echo '<script>window.location="form1.php"</script>';
                                
                            }
                            else{
                                $data = $conn->query(("INSERT INTO author_(first_name,dob,gender,address_,mobile_no,description_) VALUES('" . $fristname . "','" . $dob . "','" . $gender . "','" . $address_ . "','" . $mobile_no . "','" . $description_ . "')"));

                                if ($data) {
                    
                                    echo '<script>window.location="author.php"</script>';
                                } else {
                                    echo '<script type="text/javascript">';
                                    echo ' alert("not inserted")'; 
                                    echo '</script>';
                                    echo '<script>window.location="form1.php"</script>';
                                }
                            }
                        }
                    
                    
                    ?>


            <form action="#" method="post" id="myform" name="form">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" name="firstname" id="firstname" class="form-control aa" id="firstname" aria-describedby="emailHelp" placeholder="Enter First Name">
                      
                    </div>



                    <div class="form-group">
                        <label for="exampleInputPassword1">Date-of-Birth</label>
                        <input type="date" class="form-control" name="dob" id="dob" placeholder="Date-of-Birth">
                      
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1" class="pr-5">Select Your Gender</label>
                        <input type="radio" name="gender" checked="checked" value="male"> Male
                        <input type="radio" name="gender" value="female"> Female
                        <input type="radio" name="gender" value="other"> Other
                      
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Address</label><br>
                        <input type="text" class="form-control" id="desc1" name="desc1" placeholder="Enter your Address">
                </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Mobile No</label>
                        <input type="number" class="form-control" id="m_number" name="m_number" placeholder="Enter Mobile Number">
                   
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label><br>
                        <textarea rows="4" cols="50" id="desc" name="desc"> 
                            data is not avalable
                    </textarea>
                    </div>

                    <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

                </form> 
              


                <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
                <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
                <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
                <script>
                    // just for the demos, avoids form submit
                        
                   $("#myform").validate({
                        rules: {
                            firstname: {
                                required: true,
                                minlength: 5,
                                maxlength: 35
                            },

                            dob: {
                                required: true,
                                date: true
                            },
                            gender: {
                                required: true
                            },
                            desc1: {
                                required: true,


                            },
                            m_number: {
                                required: true,
                                number: true,
                                minlength: 10,
                                maxlength: 10
                            },
                            desc: {
                                required: true
                            }
                        }

                    });
                </script>

            </div>
        </div>
</body>

</html>