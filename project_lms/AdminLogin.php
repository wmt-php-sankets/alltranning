<?php
session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <title>Login Form</title>
</head>
<style>
    .err {
        color: #FF0000;
    }
</style>

<script>
    $(document).ready(function() {
        //EMAIL validation
        $("#email").focusout(function() {
            var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var email = $("#email").val();
            var check = pattern.test(email);
            if (email == "") {
                $(".email_error").html("Email is Empty");
            } else if (!check) {
                $(".email_error").html("Email Pattern Not Match");
            } else {

            }
        });

        // password validation

        $("#pwd").focusout(function() {
            var pwd = $("#pwd").val();

            if (pwd == "") {
                $(".password_error").html("Password is ffff Empty");
            } else {
                $(".password_error").html("");
            }
        });

        //submit validation

        $("#form").submit(function() {
            var email = $("#email").val();
            var pwd = $("#pwd").val();

            if (email == "" || pwd == "") {
                $(".email_error").html("Email is Empty");
                $(".password_error").html("Password is  Empty");
                return false;
            } else {
                return true;
            }
        });


    });
</script>

<body>

    <?php

    require "connection.php";
    $emailerr = $passwordErr = "";
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }




    if (isset($_POST['btn'])) {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $check_email = $_POST['email'];
            $check_pwd = $_POST['pwd'];
            $_SESSION['name'] = $check_email;
            if (empty($check_email)) {
                $error = true;
                $emailerr = "Required this file   d";
            } else {
                $email = test_input($check_email);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $error = true;
                    $emailerr = "*email pattern not match";
                } else {
                    $error = false;
                }
            }

            if (empty($_POST['pwd'])) {
                $error = true;
                $passwordErr = "*Required this filed";
            } else {
                $pwd = test_input($_POST['pwd']);
                if (strlen($_POST["pwd"]) <= 8) {
                    $passwordErr = "Your Password Must Contain At Least 8 Characters!";
                    $error = true;
                } else {
                    $error = false;
                }
            }
        }
        if ($error == false) {
            $qry = "select * from user_ where email='$check_email' and password='$check_pwd'";
            $result = $conn->query($qry);
            if ($result->num_rows > 0) {
                $check_email = $_POST['email'];
                $_SESSION["name"] = $_POST['email'];
                header("Location:home.php");
            } else {
                echo "<script>alert('username or password invalid')</script>";
            }
        }
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 offset-lg-3">
                <h1 class="text-center card-title">Login</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 offset-lg-3">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" id="form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control check" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">

                        <small class="email_error err"></small>
                        <small class="err"><?php echo $emailerr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control check" id="pwd" name="pwd" placeholder="Password">
                        <small class=" password_error err"></small>

                        <small class="err"><?php echo $passwordErr ?></small>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="form-control btn btn-primary" id="btn" name="btn" onclick="click()" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>
</body>

</html>