<?php require "logout.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
        .error {
            color: red;
        }
    </style>

    <title>Book_Form</title>
</head>


<body>
    <?php require "nav.php"; ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 offset-lg-3 mt-5">
                <form action="#" method="post" id="myform">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" name="title" id="title" class="form-control" aria-describedby="emailHelp" placeholder="Enter Book Title">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Pages</label>
                        <input type="number" name="pages" id="pages" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter Pages">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">langauge</label>
                        <input type="text" class="form-control" name="langauge" id="langauge" placeholder="Enter Book Langauge">
                    </div>

                    <div class="form-group">
                        <label for="">Book Author</label>
                        <select class="" name="author" id="author">
                            <option hidden>Book Author</option>
                            <?php
                            require "connection.php";
                            $qry = "select * from author_";
                            $result = $conn->query($qry);
                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                                    echo "<option value='{$row['id']}'>{$row['first_name']}</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="exampleInputPassword1">Cover Image</label>
                        <input type="file" class="form-control" id="image" name="image" placeholder="Enter Cover Image">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">ISBN Number</label>
                        <input type="text" class="form-control" name="isbn" id="isbn" placeholder="Isbn Number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label><br>
                        <textarea rows="4" cols="50" id="desc" name="desc" placeholder="Description">
</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

                </form>

                <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
                <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
                <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
                <script>
                    $("#myform").validate({
                        rules: {
                            title: {
                                required: true,
                            },
                            pages: {
                                required: true,
                                minlength: 0,
                                maxlength: 10
                            },
                            langauge: {
                                required: true,


                            },
                            author: {
                                required: true
                            },
                            image: {
                                required: true,


                            },
                            isbn: {
                                required: true,
                                number: true,
                                minlength: 10,
                                maxlength: 10
                            },
                            desc: {
                                required: true
                            }
                        }

                    });
                </script>

            </div>
        </div>
    </div>

    <?php
    require "connection.php";
    if (isset($_POST['submit'])) {
        $title = $_POST['title'];
        $pages = $_POST['pages'];
        $langauge = $_POST['langauge'];
        $author = $_POST['author'];
        $image = $_POST['image'];
        $isbn = $_POST['isbn'];
        $desc = $_POST['desc'];
        echo "$title";
        echo "$pages";
        echo "$langauge";
        echo "$author";
        echo "$image";
        echo "$isbn";
        echo "$desc";


        if ($title == "" || $pages == "" || $langauge == "" || $author == "" || $image == "" || $isbn == "" || $desc == "") {

            echo '<script type="text/javascript">';
            echo ' alert("Palse Fill above data")';
            echo '</script>';
            echo '<script>window.location="book_details.php"</script>';
        } else {

            $data = $conn->query(("INSERT INTO book_(title,pages,langauge,book_author,cover_image,isbn,description) VALUES('" . $title . "','" . $pages . "','" . $langauge . "','" . $author . "','" . $image . "','" . $isbn . "','" . $desc . "')"));

            if ($data) {

                echo '<script>window.location="book.php"</script>';
            } else {
                echo "not inserted data";
            }
        }
    }


    ?>
</body>

</html>